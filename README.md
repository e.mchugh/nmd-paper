## Introduction

This repo contains the scripts used for analysis of Illumina RNA sequencing of WT (NF54), and knockouts of nonsense-mediated decay genes (UPF1 and UPF2)  in *Plasmodium falciparum*. The `fastq.gz` files are available at the NCBI Sequence Read Archive (SRA) under accession PRJNA699307 - indiviual sample accessions are listed below:

| Sample  | Replicate | SRA accession |
| :---:   | :---:     | :---:         |
| NF54    | 1         |SRR13622007    |
| NF54    | 2         |SRR13622006    |
| NF54    | 3         |SRR13622014    |
| UPF1    | 1         |SRR13622013    |
| UPF1    | 2         |SRR13622012    |
| UPF1    | 3         |SRR13622011    |
| UPF2    | 1         |SRR13622010    |
| UPF2    | 2         |SRR13622009    |
| UPF2    | 3         |SRR13622008    |

The workflow followed is depicted below:

![](workflow.png)

Where possible, I have included `Rmarkdown` files with scripts *and* the input files required for execution. In some cases this wasn't possible, like for analyses that were performed on a cluster or require large files (`fastq.gz` or `bam`) - for Illumina read alignment (`bash-scripts/`), limma `limma.Rmd` and ASpli `aspli-analysis.Rmd`.


